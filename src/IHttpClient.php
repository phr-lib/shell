<?php 

namespace Phr\Shell;

/**
 * PHP 8.2 or above
 * 
 * @category system curl agent
 * 
 * @author Grega Lipovšček
 * @license https://lab.ortus.si
 * @link grega.lipovscek@ortus.si
 * 
 * @see HttpClient
 * 
 * >>> use Phr\Shell\Http\HttpClient
 * 
 */
interface IHttpClient
{

}