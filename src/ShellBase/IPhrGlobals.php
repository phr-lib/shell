<?php 

namespace Phr\Shell\ShellBase;

interface IPhrGlobals 
{
    public const URI = 'REQUEST_URI';
    public const METHOD = 'REQUEST_METHOD';
    public const CLIENT_IP = 'REMOTE_ADDR';  
    public const AUTHORISATION = 'Postman-Token';
}