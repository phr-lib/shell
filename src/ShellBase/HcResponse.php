<?php

namespace Phr\Shell\ShellBase; 

use Phr\Shell\IShell;

readonly class HcResponse
{
    public string $shellVersion; 

    public int $ts;
    
    public function __construct()
    {
        $this->shellVersion = IShell::VERSION;

        $this->ts = time();
    }
}