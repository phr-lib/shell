<?php

namespace Phr\Shell\Http;

interface IContentTypes 
{
    public const JSON = 'application/json; charset=utf-8';

    public const NONE = 'none';

    public const HTML = 'text/html; charset=UTF-8';

    public const FORM = 'multipart/form-data;';

}