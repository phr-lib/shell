<?php

namespace Phr\Shell\ShellBase; 

use Phr\Shell\ShellBase\IPhrGlobals as PhrGlobals;
use Phr\Shell\Http\Headers;
use Phr\Shell\IShell;

/**
 * @abstract
 * 
 * 
 * @see class  Shell
 */
abstract class ShellBase 
{   

    /**
     * @static
     * @access protected
     * @var url
     */
    protected static $url;
    /**
     * @var routes
     * Holds array of single routes
     */
    protected static array $routes;
    /**
     * @var string http method
     */
    protected static string $method;


    // CONSTRUCT ***
    public function __construct()
    {   
        self::setHeaders();
        self::setRoutes();
        self::setMethod();
        self::getAllRequestHeaders();
        self::$proccessHeader = self::getHeader();
    }

    protected static array $allowedMethods = ['GET', 'POST', 'PATCH', 'PUT'];

    protected static string $origin = "*";

    protected static bool $cridentials = true;

    protected static bool $closeConnetion = false;

    protected static string|null $proccessHeader;

    protected static string|null $responseHeader = null;

    private static array $requestHeaders = [];

    protected static function allowMethods(): string 
    {
        return implode(',', self::$allowedMethods);
    }
    protected static function getHeader(string $_header_key = IShell::PROCESS_HEADER_KEY )
    {
        if (isset(self::$requestHeaders[$_header_key])) {
            return trim(self::$requestHeaders[$_header_key]);
        }
    }
    
    /**
     * @access protected
     * @method converts url into routes
     * array. 
     * @param int beforeIndex is start pointer.
     * If it is nead to set entry point in another
     * folder or subfolder, before index shuld be 
     * increased.
     * @param int after index sets nubers of readed 
     * paths.
     */
    protected static function setRoutes(int $_beforeIndex = 1, int $_afterIndex = 5): void 
    {          
        self::setUrl();

        $PhraseUri = explode(DIRECTORY_SEPARATOR, self::$url);
        
        self::$routes = array_slice($PhraseUri,$_beforeIndex,$_afterIndex);   
    }
    protected static function getLastUrlParameters(): string
    {   
        if(self::$url)
        {
            $par = explode('/', self::$url);
            $readLastParameterInUrl = array_pop($par);
        }  
        return $readLastParameterInUrl;
    } 
     /**
     * 
     * @access private
     * @method catches url routes
     */
    private static function setUrl(): void 
    {
        self::$url = $_SERVER[PhrGlobals::URI];
    }
    private static function setMethod(): void
    {
        self::$method = $_SERVER[PhrGlobals::METHOD];
    }
    private static function setHeaders(): void
    {         
        Headers::accessControlAllowHeaders();     
        Headers::accessControlOrigin(self::$origin);
        Headers::accessControlAllowCridentials(self::$cridentials);
    }
    /**
     * @method gets all rewquest 
     * headers.
     * @return array of headers.
     */
    private static function getAllRequestHeaders(): void
    {
        if (function_exists('apache_request_headers')) 
        {
            $requestHeaders = apache_request_headers();
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
        }     
        self::$requestHeaders = $requestHeaders;
    }
     
     
}

