<?php 

namespace Phr\Shell;

use Phr\Shell\Http\ResponseCode;

/**
 * PHP 8.2 or above
 * 
 * @category system shell
 * 
 * @author Grega Lipovšček
 * @license https://lab.ortus.si
 * @link grega.lipovscek@ortus.si
 * 
 * @see shell
 * 
 */
interface IShell 
{   
    public const VERSION = 'v1.0.1';
    /**
     * @access public
     * @var const setting for error costume
     * header response. 
     * @see errorResponse()
     */
    public const ERROR_RESPONSE_HEADER_KEY = "Phr-Error";
    /**
     * @var string process key.
     */
    public const PROCESS_HEADER_KEY = "Phr-Process";
    /**
     * @var string response header key.
     */
    public const RESPONSE_HEADER_KEY = "Phr-Response";

    public const AUTHORIZATION_HEADER_KEY = "Encry-Token";

    /**
     * @static
     * @method route
     * @param int selected route level
     * Select route level from shellbase static routes
     * @return string|null route level or no level
     */
    public static function route( int $_selected_route_index = 0 ): string | null;
    /**
     * @method returns full route
     * @return string|null
     */
    public static function routes(): string|null;
    /**
     * @method access SERVER varibile for method
     * @return string method GET? POST? DELETE?
     */
    public static function method(): string;
    /**
     * @method send json respons
     * @param ResponseCode response code
     * @param object|string|null $content to responde
     * @return json string
     */
    public static function response( int $_response_code = ResponseCode::NOCONTENT, object|string|null $_content = null ): void;
    /**
     * @method sets response status code to 
     * exception intiger error. If there is error 
     * message int prints it to response headers. 
     * Default header key value:
     * @see const ERROR_RESPONSE_HEADER_KEY
     */
    public static function errorResponse( $error ): void;
    /**
     * @method starts new session
     * @param string sessionId for session indetificator
     * @param content undefinetd - accepts all sorts of session content. 
     * @param bool close - if session is to be closed after writeing it.
     * @param int lifetime - ttl or time to live 
     */
    public static function session( string $_sessionId, $_content, bool $_close = true, int $_lifeTime = 86400 ): void ;
    /**
     * @method catches incomeing
     * data burst 
     * 
     * @return json
     */
    public static function incomeing();
    /**
     * @method catches incomeing json
     * data burst and decode it to json
     * object
     * @return json
     */
    public static function posted();
    /**
     * @method sets cooke
     * @param string cooke id key
     * @param string|int cooke value to store
     * @param int duratin - ttl
     */
    public static function cookie( string $_cookie, string|int $_value, int $_duration = 3600): void;
    
    /**
     * @method add delete method. 
     * Default delete method is not includen
     * in access controll
     * @param string method | Default DELETE
     */
    public static function addMethod(string $_method = 'DELETE'): void;
    /**
     * @method sets Phr-Respone header.
     * @param string header text.
     */
    public static function setResponseHeader(string $_response_header_text): void;
    /**
     * @method fetches Encry-Token bearer
     * @return string|null Encry-Token bearer
     */
    public static function authorize(): string |null; 


}