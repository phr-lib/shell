<?php

namespace Phr\Shell\ShellBase; 


class Authorization
{   
    /**
     * @static
     * @access public 
     * @method for authorisation header ! 
     * @return string|null for authorisation bearer token or null
     * 
     */
    public static function extractBearer( string $_header ): string|null 
    {        
        if (!empty($_header)) 
        {
            if (preg_match('/Bearer\s(\S+)/', $_header, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
    /**
     * @static
     * @access private
     * Backup function for different authorisation app
     * @return string headers
     * 
     */
    private static function getAuthorizationHeader(): string|null
    {
        $header = null;
        if (isset($_SERVER['Authorization'])) {
            $header = trim($_SERVER["Authorization"]);
        }
        elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $header = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } 
        elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));

            if (isset($requestHeaders['Authorization'])) {
                $header = trim($requestHeaders['Authorization']);
            }
        }     
        return $header;
    }
    private static function getHeader(string $_header_key): string|null
    {
        $headers = null;
       
        if (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));

            if (isset($requestHeaders[$_header_key])) {
                $headers = trim($requestHeaders[$_header_key]);
            }
        }     
        return $headers;
    }
}