<?php

namespace Phr\Shell\Http;

interface ResponseCode 
{
    public const OK = 200;

    public const CREATED = 201;

    public const ACCEPTED = 202;

    public const NOCONTENT = 204;

    public const BADREQUEST = 400;

    public const UNAUTHORIZED = 401;

    public const FORBIDDEN = 403;

    public const METHODNOTALLOWED = 405;

    public const NOTACCEPTABLE = 406;

    public const CONFLICT = 409;

    public const GONE = 410;

    public const EXPECTATIONFAILED = 417;

    public const INTERNALSERVERERROR = 500;

    public const NOTIMLEMENTED = 501;

    public const BADGATEWAY = 502;

    public const SERVICEUNAVAIABLE = 503;


}
