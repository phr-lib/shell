<?php

namespace Phr\Shell\Http;

use Phr\Shell\IHttpClient;

abstract class HttpClientBase 
{   
    
    /**
     * @access protected
     */
    protected $curl;

    protected $response;

    protected string $basicUrl;

    ### CONSTRUCTOR ***
    public function __construct(string $_url)
    {
        $this->basicUrl = $_url;
    }

    /**
     * @method init
     * @var enpoint 
     * Start curl
     */
    protected function init( string $_endpoint ):void 
    {   
        $FullUrl = $this->basicUrl.DIRECTORY_SEPARATOR.$_endpoint;
        $this->curl = curl_init(); 
        
        curl_setopt($this->curl, CURLOPT_URL, $FullUrl );   
          
    }

    /**
     * @method exec
     * Executes curl
     */
    protected function exec()
    {   
        
        $output = curl_exec($this->curl);
        curl_close($this->curl);
        $this->response = $output;
    }
}

class HttpClient extends HttpClientBase implements IHttpClient
{
    public function headers()
    {
        $headers = [
            'X-headerone: 0',
            'X-header2: sigma',
        ];
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
    }
    
    public function get( string $_endpoint )
    {   
        $this->init($_endpoint); 
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        
        $this->exec();
        return $this->response;
    }

    /**
     * @method post
     * @var array data
     * @return curl result
     */
    public function post( string $_endpoint, $data) 
    {   
        $JsonData = json_encode($data);
        $this->init($_endpoint); 
        curl_setopt($this->curl, CURLOPT_POST, 1); 
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $JsonData);
        $this->exec();
        return $this->response;
    }
    
}