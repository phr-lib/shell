<?php

namespace Phr\Shell\Http;

interface IHttpMethod 
{
    public const GET = 'GET';

    public const POST = 'POST';

    public const DELETE = 'DELETE';

    public const UPDATE = 'UPDATE';

    public const PATCH = 'PATCH';

    public const PUT = 'PUT';

}