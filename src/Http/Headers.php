<?php

namespace Phr\Shell\Http;

use Phr\Shell\Http\IContentTypes;

/**
 * Headers
 * 
 */
class Headers implements IContentTypes
{
    public static function headersContentJson()
    {
        header('Content-Type: ' . IContentTypes::JSON );    
    }
    public static function accessControlOrigin( string $_set_access = "*")
    {
        header('Access-Control-Allow-Origin: ' . $_set_access);    
    }
    public static function accessControlMethod( string $_set_methods = "GET")
    {
        header('Access-Control-Allow-Methods: ' . $_set_methods);    
    }
    public static function accessControlAllowCridentials( bool $_allow_cridentials = true)
    {
        header('Access-Control-Allow-Credentials: ' . $_allow_cridentials);    
    }
    public static function accessControlAllowHeaders( string $_allow_headers = "*")
    {
        header('Access-Control-Allow-Headers: ' . $_allow_headers);    
    }
    public static function contentType( string $_content_type )
    {
        header('Content-Type: ' . $_content_type );
    }
    public static function headerMessage( string $_header_message )
    {
        header('Api-message: ' . $_header_message );
    }
    public static function connectionClose()
    {
        header('Connection: close');
    }
}