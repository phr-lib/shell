<?php 

namespace Phr\Shell;

use Phr\Shell\ShellBase\ShellBase;
use Phr\Shell\ShellBase\Authorization;
use Phr\Shell\ShellBase\IPhrGlobals as PhrGlobals;
use Phr\Shell\Http\ResponseCode;
use Phr\Shell\Http\Headers;
use Phr\Shell\Http\IContentTypes;

/**
 * @final
 * 
 * 
 * @see interface IShell
 */
final class Shell extends ShellBase implements IShell
{    
    private static string $selectedRoute;

    public static function route( int $_selected_route_index = 0 ): string | null
    {   
        if( count(self::$routes) <= $_selected_route_index ) return null;
        if($_selected_route_index > 0) self::$selectedRoute = self::$routes[$_selected_route_index];
        return self::$routes[$_selected_route_index];
    }
    public static function routes(): string|null
    {
        return (isset(self::$url)) ? self::$url: null;
    }
    public static function method(): string
    {
        return self::$method;
    }  
    public static function response( int $_response_code = ResponseCode::NOCONTENT, object|string|null $_content = null ): void
    {  
        Headers::accessControlMethod(self::allowMethods());
        http_response_code($_response_code);
        if(self::$proccessHeader != null) header(IShell::PROCESS_HEADER_KEY .':'.self::$proccessHeader);
        if(self::$responseHeader != null) header(IShell::RESPONSE_HEADER_KEY .':'.self::$responseHeader);
        if(self::$closeConnetion == true) Headers::connectionClose();
        if($_content == null)
        {   
            Headers::contentType(Headers::NONE);
            exit();
        }else
        {   
            Headers::headersContentJson();
            print( json_encode( $_content ) );
            exit();
        }
    }
    public static function secureResponse( object|string $_content, int $_response_code = ResponseCode::OK): void
    {  
        Headers::accessControlMethod(self::allowMethods());
        http_response_code($_response_code);
        if(self::$proccessHeader != null) header(IShell::PROCESS_HEADER_KEY .':'.self::$proccessHeader);
        if(self::$responseHeader != null) header(IShell::RESPONSE_HEADER_KEY .':'.self::$responseHeader);
        if(self::$closeConnetion == true) Headers::connectionClose();
        Headers::contentType(IContentTypes::HTML);
        print( $_content );
        exit();
    }
    public static function addMethod(string $_method = 'DELETE'): void 
    {
        array_push(self::$allowedMethods, $_method);
    }
    public static function errorResponse( $error ): void
    {
        http_response_code( $error->getCode() );
        if(self::$proccessHeader != null) header(IShell::PROCESS_HEADER_KEY .':'.self::$proccessHeader);
        if($error->getMessage())
            header(IShell::ERROR_RESPONSE_HEADER_KEY.
            ": ".
            $error->getMessage());

        header(Headers::contentType(Headers::NONE)); 
        exit();
    }
    public static function session( string $_sessionId, $_content, bool $_close = true, int $_lifeTime = 86400 ): void 
    {
        session_start([
            'cookie_lifetime' => $_lifeTime,
            'read_and_close'  => $_close,
        ]);
        $_SESSION[$_sessionId] = $_content;
    }
    public static function incomeing()
    {
        if(file_get_contents('php://input'))
            {
                return file_get_contents('php://input');
            }
    }
    public static function posted()
    {
        if(file_get_contents('php://input'))
            {
                return json_decode(self::incomeing());
            }
    }
    public static function cookie( string $_cookie, string|int $_value, int $_duration = 3600): void 
    {
        setcookie($_cookie, $_value, time()+$_duration);
    }
    public static function parameters(): string|int|null
    {   
        $parameter = parent::getLastUrlParameters();
        if(isset(self::$selectedRoute)){
            if(self::$selectedRoute == $parameter) return null;
            else return $parameter ? $parameter: null;
        }
        else return $parameter ? $parameter: null;
    }
    public static function process(): string|null
    {   
        return self::$proccessHeader;
    }
    public static function setResponseHeader(string $_response_header_text): void 
    {
        self::$responseHeader = $_response_header_text;
    }
    public static function authorize(): string |null
    {   
        return Authorization::extractBearer( self::getHeader(self::AUTHORIZATION_HEADER_KEY) );
    }

   
    
}